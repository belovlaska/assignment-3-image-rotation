#ifndef VALIDATION_H
#define VALIDATION_H

enum validation_status {
    VALID = 0,
    INVALID_NUMBER_OF_ARGUMENTS = 1,
    INVALID_ANGLE = 2,
    ANGLE_OUT_OF_ARRAY = 3
};

enum validation_status validate(int argc, char** argv, long* angle);


#endif //VALIDATION_H
