
#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

struct image rotate (struct image *img, int64_t angle);

#endif //ROTATE_H
