#include "validation.h"
#include <stdlib.h>


enum validation_status validate(int argc, char** argv, long* angle) {
    if (argc != 4) {
        return INVALID_NUMBER_OF_ARGUMENTS;
    }

    char *p;
    long converted = strtol(argv[3], &p, 10);
    if (*p != '\0') {
        return INVALID_ANGLE;
    }

    *angle = (360 - converted) % 360;
    if (*angle % 90 != 0 || *angle > 360) {
        return ANGLE_OUT_OF_ARRAY;
    }

    return VALID;
}
