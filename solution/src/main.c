
#include <stdio.h>
#include <string.h>

#include "image.h"
#include "bmp_handler.h"
#include "rotate.h"
#include "validation.h"


enum exit_status {
    SUCCESS = 0,
    INVALID_DATA,
    OPEN_FILE_ERROR,
    SIGNATURE_ERROR,
    INVALID_HEADER,
    INVALID_BITS,
    CLOSE_ERROR,
    CANT_SAVE_ERROR
};

int main(int argc, char** argv) {
    long angle;

    enum validation_status valid = validate(argc, argv, &angle);
    if(valid){
        printf("Invalid data\n");
        return INVALID_DATA;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("Failed to open source file\n");
        return OPEN_FILE_ERROR;
    }

    struct image img;
    enum read_status rs = from_bmp(in, &img);

    switch (rs) {
        case READ_OK:
            printf("File loaded successfully\n");
            break;
        case READ_INVALID_SIGNATURE:
            fclose(in);
            printf("Invalid signature\n");
            return SIGNATURE_ERROR;
        case READ_INVALID_HEADER:
            fclose(in);
            printf("Invalid header\n");
            return INVALID_HEADER;
        case READ_INVALID_BITS:
            fclose(in);
            printf("Invalid bits\n");
            return INVALID_BITS;
        case READ_ERROR:
            fclose(in);
            printf("Reading error\n");
            return READ_ERROR;
    }

    if (fclose(in) != 0) {
        printf("Failed to close source file\n");
        return CLOSE_ERROR;
    }


    struct image result = rotate(&img, angle);

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        printf("Failed to open target file\n");
        return OPEN_FILE_ERROR;
    }

    enum write_status ws = to_bmp(out, &result);

    switch (ws) {
        case WRITE_OK:
            printf("Image saved\n");
            break;
        case WRITE_ERROR:
            printf("Failed to save image\n");
            return CANT_SAVE_ERROR;
    }

    ustroy_destroy_img(&result);

    if (fclose(out) != 0) {
        printf("Failed to close target file\n");
        return CLOSE_ERROR;
    }
    return SUCCESS;
}
