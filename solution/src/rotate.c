#include "rotate.h"

#define For(x, n) for(uint64_t (x) = 0; (x) < (n); ++(x))

struct image rotate_90degrees (struct image *img) {
    struct image result_img = make_img(img->height, img->width);

    For(x, img->height){
        For(y, img->width){
            result_img.data[(y + 1) * result_img.width - x - 1] = img->data[x * img->width + y];
        }
    }
    ustroy_destroy_img(img);
    return result_img;
}

struct image rotate (struct image *img, int64_t angle) {\
    uint64_t times = angle >= 0 ? angle / 90 : 4 + angle / 90;
    if (angle == 0) return *img;

    struct image result = *img;
    For(i, times){
        result = rotate_90degrees(&result);
    }
    return result;
}
